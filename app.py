from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/get')
def get_request():
    return 'OHAI!'

@app.route('/post', methods=['GET', 'POST'])
def post_request():
    return 'you submitted a key of %s with a value of %s' % (request.form.keys(), request.form.values())

if __name__ == '__main__':
    app.run(debug=True)
